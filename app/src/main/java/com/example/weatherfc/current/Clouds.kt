package com.example.weatherfc.current


import com.google.gson.annotations.SerializedName

data class Clouds(
    val all: Int
)