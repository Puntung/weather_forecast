package com.example.weatherfc.current.api

import com.example.weatherfc.current.currentWT
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

val BASE_URL = "https://api.openweathermap.org/data/2.5/"

interface ApiService{
    @GET("weather")
    fun getCurrentWeather(@Query("q") city:String,@Query("appid") appid:String = "6063175ff00ea8fff81d5a211d2b59cd"):Call<currentWT>

    companion object{
        operator fun invoke():ApiService{
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService::class.java)
        }
    }
}