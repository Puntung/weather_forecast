package com.example.weatherfc

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.weatherfc.current.api.ApiService
import com.example.weatherfc.current.currentWT
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal
import java.text.NumberFormat



class MainActivity : AppCompatActivity() {
//    Developed By Nakarin Puntung
    private val apiKey = "6063175ff00ea8fff81d5a211d2b59cd"
    private var currentUnit:String = "C"
    lateinit private var apiService:ApiService
    private var latLong = LatLong("0","0")
    private var progressDialog: ProgressDialog? = null
    private var spinner: Spinner? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        search_btn.setOnClickListener {
            this.currentUnit = "C"
            callCurrent()
        }
        cToF.setOnClickListener {
            switchUnit()
        }
        apiService = ApiService()
        wholeday.setOnClickListener {
            goToWholeDay()
        }
    }
    private fun goToWholeDay(){

        val city = yourCity.text
        val intent = Intent(this, MainActivity2::class.java)
        intent.putExtra("latlng", this.latLong)
        startActivity(intent)
    }
    private fun switchUnit(){
        val nf = NumberFormat.getInstance()
        val temp = temp_txt.text
        if(temp == "0" || temp == "null"){
            return
        }
        if(currentUnit == "C"){
            val celcius = nf.parse(temp as String).toDouble()
            val result = (celcius*1.8)+32
            temp_txt.text = result.toString().substring(0,5)+"ํ F"
            cToF.text = "Switch to Celcius"
            this.currentUnit = "F"
        }
        else if(currentUnit == "F"){
            val farenheit = nf.parse(temp as String).toDouble()
            val result = (farenheit-32)*0.5556
            temp_txt.text = result.toString().substring(0,5)+"ํ C"
            cToF.text = "Switch to Farenheit"
            this.currentUnit = "C"
        }
    }
    private fun callCurrent() {
        spinner = findViewById(R.id.spinner) as Spinner
        hideKeyboard()
        currentWeather()
    }
    private fun currentWeather(){
        progressDialog = ProgressDialog(this@MainActivity)
        progressDialog!!.setMessage(getString(R.string.loading))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
        val city = yourCity.text
        val call = apiService.getCurrentWeather("$city")
        call.enqueue(object:Callback<currentWT>{
            override fun onFailure(call: Call<currentWT>, t: Throwable) {
                progressDialog!!.dismiss()
                Log.e("ERROR",t.message)
            }

            override fun onResponse(call: Call<currentWT>, response: Response<currentWT>) {
                progressDialog!!.dismiss()
               if(response.isSuccessful){
                   weatherTerm.visibility = View.VISIBLE
                   wholeday.visibility = View.VISIBLE
                   val list = response.body()
                   Log.i("API","${list}")
                   val celcius : BigDecimal = (BigDecimal.valueOf(list!!.main!!.temp) - BigDecimal.valueOf(273.15))
                   Log.i("result","${celcius}")
                   temp_txt.text = celcius.toString()+" ํC"
                   humid_txt.text = list!!.main!!.humidity.toString()+"%"
                   this@MainActivity.currentUnit = "C"
                   this@MainActivity.latLong = LatLong("${list!!.coord!!.lat}","${list!!.coord!!.lon}")
                   Log.v("Latlng",this@MainActivity.latLong.lat.toString())
               }
                else{
                   Toast.makeText(this@MainActivity,"city not found",Toast.LENGTH_LONG).show()
                   temp_txt.text = "null"
                   humid_txt.text = "null"

                   wholeday.visibility = View.INVISIBLE
               }
            }
        })
    }
    private fun hideKeyboard(){
        val view = this.currentFocus
        if (view != null){
            val hideMe = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            hideMe.hideSoftInputFromWindow(view.windowToken,0)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
    //Develop by Nakarin Puntung
}