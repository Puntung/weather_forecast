package com.example.weatherfc.wholeday

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

val BASE_URL = "https://api.openweathermap.org/data/2.5/"

interface WholeDayService{
    @GET("onecall")
    fun getWholeDayForecast(@Query("lat") lat:String,@Query("lon") lon:String,@Query("exclude") exclueding:String = "daily",@Query("appid") appid:String = "6063175ff00ea8fff81d5a211d2b59cd"):Call<wholeDayResponse>

    companion object{
        operator fun invoke():WholeDayService{
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WholeDayService::class.java)
        }
    }
}