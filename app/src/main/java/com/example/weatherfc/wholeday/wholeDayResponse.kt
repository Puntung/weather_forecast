package com.example.weatherfc.wholeday


import com.google.gson.annotations.SerializedName
//    Developed By Nakarin Puntung
data class wholeDayResponse(
    val current: Current,
    val hourly: List<Hourly>,
    val lat: Double,
    val lon: Double,
    val timezone: String,
    @SerializedName("timezone_offset")
    val timezoneOffset: Int
)