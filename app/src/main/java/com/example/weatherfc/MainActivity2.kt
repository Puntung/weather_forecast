package com.example.weatherfc

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.weatherfc.wholeday.WholeDayService
import com.example.weatherfc.wholeday.wholeDayResponse
import kotlinx.android.synthetic.main.activity_main2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

class MainActivity2 : AppCompatActivity() {
    //    Developed By Nakarin Puntung
    private var progressDialog: ProgressDialog? = null
    private var spinner: Spinner? = null
    lateinit var apiService: WholeDayService
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val getLatLong = intent.getSerializableExtra("latlng") as LatLong
        apiService = WholeDayService()
        spinner = findViewById(R.id.spinner) as Spinner

        getWholeDay("${getLatLong.lat.toString()}", "${getLatLong.long.toString()}")
    }

    private fun getWholeDay(lat: String, long: String) {
        progressDialog = ProgressDialog(this@MainActivity2)
        progressDialog!!.setMessage(getString(R.string.loading))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
        val call = apiService.getWholeDayForecast("$lat", "$long")
        call.enqueue(object : Callback<wholeDayResponse> {
            override fun onFailure(call: Call<wholeDayResponse>, t: Throwable) {
                progressDialog!!.dismiss()
                Log.v("Error", t.message)
            }

            override fun onResponse(
                call: Call<wholeDayResponse>,
                response: Response<wholeDayResponse>
            ) {
                progressDialog!!.dismiss()
                if (response.isSuccessful) {
                    val result = response.body()
                    Log.v("Result", result!!.hourly.toString())
                    creatTempTerm(result)
                    city_txt.text = result!!.timezone.toString()
                }
                else{
                    Toast.makeText(this@MainActivity2,"Content not loading now",Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun creatTempTerm(result: wholeDayResponse) {
        for (i in 0 until result!!.hourly!!.size) {
            val timeView = TextView(this@MainActivity2)
            val tempView = TextView(this@MainActivity2)
            val humidView = TextView(this@MainActivity2)
            val mainView = TextView(this@MainActivity2)
            val timeST = "Datetime : ${getTime("${result!!.hourly[i]!!.dt}")}"
            timeView.text = timeST
            timeView.textSize = 18f
            timeView.setPadding(0, 20, 0, 0)
            val temperature: BigDecimal =
                BigDecimal.valueOf(result!!.hourly[i]!!.temp) - BigDecimal.valueOf(273.15)
            tempView.text = "Temperature : ${temperature} C"
            tempView.textSize = 14f
            humidView.text = "Humidity : ${result!!.hourly[i]!!.humidity} %"
            humidView.textSize = 14f
            mainView.text = "Weather : ${result!!.hourly[i]!!.weather[0]!!.description} \n\n"
            mainView.textSize = 14f
            temp_term.addView(timeView)
            temp_term.addView(tempView)
            temp_term.addView(humidView)
            temp_term.addView(mainView)
        }
    }

    private fun getTime(s: String): String? {
        try {
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm")
            val netDate = Date(s.toLong() * 1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }
    //    Developed By Nakarin Puntung
}