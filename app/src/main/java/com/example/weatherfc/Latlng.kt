package com.example.weatherfc

import java.io.Serializable

data class LatLong(
    val lat:String,
    val long:String
): Serializable